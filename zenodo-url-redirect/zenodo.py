"""
A python module that connects to a zenodo URL and replicates 
this sequence of CURL commands:

curl -s -L -D - https://doi.org/10.5281/zenodo.3896797 -o /dev/null | grep Location:
curl -i https://zenodo.org/api/records/3896805
curl https://zenodo.org/api/files/0abc6a92-c956-4788-b41b-b7a3f8e05c04/dataset_loc.txt
"""
import os, requests, argparse, sys, json

parser = argparse.ArgumentParser(description="Retreive resource URLs from a zenodo dataset_loc file")
parser.add_argument("url", type=str, help="A zenodo URL")

ZAPI = "https://zenodo.org/api/records/"

# URL = "https://doi.org/10.5281/zenodo.3896797"

def _getRecordID(url):
    parts = url.split(sep="/")
    id = parts[-1]
    if len(id) < 3:
        raise Exception("Record ID should be a longer string of numbers. Got: {}".format(id))
    return id

def _isURL(url):
    if url[:4] == "http":
        return True
    else: 
        return False

def _checkForZenodo(url):
    if not _isURL(url):
        raise Exception("The URL does not start with http. Suspicious. ({})".format(url))
    if "zenodo" not in url:
        raise Exception("The URL is not a zenodo URL ({})".format(url))

def _checkInputURL(url):
    args = parser.parse_args()
    if url is None:
        url = args.url
        if url is None:
            raise Exception("no url provided!")
    _checkForZenodo(url)
    return url

def _getLatestURL(startURL, timeout=3):
    response = requests.get(startURL, timeout=timeout)
    return response.url

def _getURL(url=None):
    url = _checkInputURL(url)
    latestURL = _getLatestURL(url)
    return latestURL

def _getZenodoFileList(zdict):
    files = zdict["files"]
    return files

def getRealDataLocFromZAPI(url=None, timeout=3):
    url = _getURL(url)
    id = _getRecordID(url)
    info = requests.get(ZAPI+id, timeout=timeout)
    jstr = info.content
    jdict = json.loads(jstr)
    files = _getZenodoFileList(jdict)
    realLocs = []
    for foundFile in files:
        fileName = foundFile["links"]["self"]
        if "dataset_loc" in fileName and fileName.endswith(".txt"):
            urlFromFile = requests.get(fileName, timeout=timeout).content.decode("utf-8").strip()
            realLocs.append(urlFromFile)

    return realLocs


if __name__ == "__main__":
    sys.exit(getRealDataLocFromZAPI())